# -*- coding: utf-8 -*-

import sys, os, io, codecs, shutil
import cyrtranslit, hashlib, binascii


ignoreKeywords = ['.img','.directory','index.md','LICENSE','README.md','404.md']
source = 'source'
content = 'content'

def absoluteFilePaths(directory, includeFolders):
	lst = []
	for root, dirs, files in os.walk(os.path.abspath(directory)):
		for file in files:
			path = os.path.join(root, file)
			pathname = os.path.abspath(os.path.dirname(sys.argv[0]))
			path = path[len(pathname)+len(directory)+2:]
			lst.append(path)

	for i in range(len(lst)):
		for word in ignoreKeywords:
			if lst[i].find(word) != -1:
				lst[i] = ""

	newList = []
	for path in lst:
		if path != "":
			newList.append(path)
	return newList

def getHashes(lst):
	hashList = []
	for path in lst:
		hash = hashlib.sha1(path).hexdigest()
		hashList.append(hash)
	return hashList

def lat(s):
	latin = cyrtranslit.to_latin(s, 'ru')
	latin = latin.replace(' ','_')
	latin = latin.replace('\'','')
	latin = latin.replace('\"','_')
	latin = latin.replace('#','_')
	latin = latin.replace('?','')
	latin = latin.replace('&','_')
	latin = latin.replace('!','')
	latin = latin.replace('|','_')
	return latin

def getLatinPaths(lst):
	pathList = []
	for path in lst:
		pathList.append(lat(path))
	return pathList

def listToStrConverter(lst):
	text = ""
	lst = lst[::-1]
	for s in lst:
		text += s + '\n'
	return text

def listUnion(l1, l2):
	newList = []
	for i in range(len(l1)):
		newList.append(l1[i] + '|' + l2[i])
		
	return newList

paths = absoluteFilePaths(source, False)
hashes = getHashes(paths)
latinPaths = getLatinPaths(paths)

tNavigation = listToStrConverter(paths)
tStructure = listToStrConverter(listUnion(hashes, latinPaths))

with open("navigation.txt", "w") as text_file:
	text_file.write(tNavigation)
    
with open("structure.txt", "w") as text_file:
	text_file.write(tStructure)

def copySelectedList(lst):
	if os.path.exists(content):
		shutil.rmtree(content)
	shutil.copytree(source, content)

def renameFolders(root):
	for item in os.listdir(root):
		if os.path.isdir(root+'/'+item):
			renameFolders(root+'/'+item)
		print(root+'/'+item)
		shutil.move(root+'/'+item, root+'/'+lat(item))
    
copySelectedList(paths)
renameFolders(content)
