function repeatWhileMouseOver(element, action, time) {
    var interval = null;
    element.addEventListener('mouseover', function() {
        interval = setInterval(action, time);
    });

    element.addEventListener('mouseout', function() {
        clearInterval(interval);
    });
}  

var mouseY = 0;
function scroll() {
    limit = 0.15;
    var elementOffsetY = $("#navigation").offset().top;
    var elHeight = $('#navigation').height();
    var pct = (mouseY - elementOffsetY)/elHeight;
    
    if (pct < limit) {
        _e('navigation').scrollTop -= 3;
    }
    if (pct > 1-limit) {
        _e('navigation').scrollTop += 3;
    }
}

document.addEventListener('mousemove', onMouseUpdate, false);
document.addEventListener('mouseenter', onMouseUpdate, false);
function onMouseUpdate(e) {
    mouseY = e.pageY;
}
